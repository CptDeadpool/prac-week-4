﻿using UnityEngine;
using System.Collections;

public class Player1Move : MonoBehaviour {

	public float maxSpeed = 5.0f;

	public Vector2 velocity;
	public Vector2 velocity2;
	
	// Update is called once per frame
	void Update () {

		Vector2 direction; 
		direction.x = Input.GetAxis ("Horizontal");
		direction.y = Input.GetAxis ("Vertical");	

		Vector2 velocity = direction * maxSpeed;

		transform.Translate (velocity * Time.deltaTime);
	}
}
